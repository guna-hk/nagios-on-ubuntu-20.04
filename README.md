# Install nagios on ubuntu 20.04

last update 17/9/2020

## Method 1

use the install script in this repo

```bash
mkdir nagios
cd nagios
wget https://gitlab.com/guna-hk/nagios-on-ubuntu-20.04/-/raw/master/install.sh
chmod +x install.sh
sudo ./install.sh
cd ..
sudo rm -rf nagios
```

## Method 2

https://kifarunix.com/install-and-setup-nagios-core-on-ubuntu-20-04/


# Installing agents

https://www.tecmint.com/how-to-add-linux-host-to-nagios-monitoring-server/