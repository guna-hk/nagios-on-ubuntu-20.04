#!/usr/bin/env bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi
read -p "Enter the version of Nagios you want to install (default is 4.4.6):" NAGIOS_VER
NAGIOS_VER=${NAGIOS_VER:-"4.4.6"}
IP=$(hostname -I)
IP=${IP%?}
apt update
yes | apt upgrade
apt install autoconf gcc libc6 make wget unzip apache2 php libapache2-mod-php libgd-dev -y
wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-$NAGIOS_VER.tar.gz
tar xzf nagios-$NAGIOS_VER.tar.gz
cd nagios-$NAGIOS_VER
./configure --with-httpd-conf=/etc/apache2/sites-enabled
make all
make install-groups-users
usermod -aG nagios www-data
make install
make install-init
make install-daemoninit
make install-commandmode
make install-config
make install-webconf
a2enmod rewrite
a2enmod cgi
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
systemctl enable --now nagios
systemctl restart apache2
systemctl enable --now apache2
yes | apt install nagios-plugins
ln -s /usr/lib/nagios/plugins/* /usr/local/nagios/libexec
echo "Installation finished"
echo "You can now access Nagios Core from web interface via, https://$IP/nagios."
echo "web interface username: nagiosadmin"
exit
